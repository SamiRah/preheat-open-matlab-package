classdef ControlUnit < handle & PreHEAT_API.BuildingUnit
    %ControlUnit Defines a control unit in the PreHEAT sense
    
    properties (GetAccess='public', SetAccess='protected')
        % All inherited
    end
    properties (GetAccess='private', SetAccess='protected')
        active = NaN;
    end
    
    methods (Access='public')
        
        function obj = ControlUnit(input_data, parent_building, parent_unit)            
            % Constructor creating a Component based upon a component id or
            % a structure.
            %
            % Inputs:
            %       input_data  Description of the component, either
            %                       component id (integer)
            %                           or
            %                       structure ( struct(cid,name) )
            %
            %       parent_building     Handle to parent building
            %       
            %       unit_type           Type of the unit (string)
            %
            %       parent_unit         Parent unit (PreHEAT_API.BuildingUnit)
            %
            if nargin<2
                parent_building = [];
            end
                        
            if nargin<3
                parent_unit = [];
            end
            
            % Using constructor of parent class
            fields2ignore = {'active'};
            obj@PreHEAT_API.BuildingUnit( input_data, parent_building, 'controlUnit', parent_unit, fields2ignore );
            
            % Adding the active field
            if isfield(input_data,'active')
                obj.active = input_data.active;
            end
        end
                        
        function [call_result]  = setSchedule(obj, schedule, fallback_value, fallback_startTime)
            % Method to write a schedule to the given control unit
            %
            % Inputs:
            %       schedule            Timetable containing the schedule
            %                               (columns: startTime, value, [operation])
            %       fallback_value      Fallback value to use after the median timestep (optional - default: none)
            %       fallback_startTime  Start time of fallback value (optional - default: end + median timestep in schedule)
            %
            % Outputs:
            %       /               The data is loaded in the data attribute of the current object
            %
            
            
            if ~istimetable(schedule) || isempty(schedule)
               error('Illegal format for "schedule" input parameter: must be a non-empty timetable'); 
            end
            if nargin<3
                fallback_value = [];
            end
            if nargin<4
                fallback_startTime = [];
            end
            
            N = height(schedule);
                
            if ismember('operation',schedule.Properties.VariableNames)
                operation_mode = schedule.operation;
            else
                % Else auto-filled with normal
                operation_mode = cell(N,1);
                for i=1:N
                    operation_mode{i} = 'NORMAL';
                end
            end
            
            values = num2cell(schedule.value);
            start_times = cellstr( PreHEAT_API.ApiConfig.convertToApiInputDate(schedule.startTime) );
            
            % Adding the fallback value
            if ~isempty(fallback_value)
                if isempty(fallback_startTime)
                    fallback_startTime = schedule.startTime(end) + median(diff(schedule.startTime));
                end
                
                start_times{end+1}      = PreHEAT_API.ApiConfig.convertToApiInputDate(fallback_startTime);
                values{end+1}           = fallback_value;
                operation_mode{end+1}   = 'NORMAL';
            end
            
            schedule2use = struct( ...
                'value',        values,...
                'startTime',    start_times,...
                'operation',    operation_mode );
            
            schedule2write = struct( 'schedule', schedule2use );
            
            % Check that the unit is active
            if obj.active==0 || isnan(obj.active)
                warning('The control unit (%i) is not active. The schedule will therefore not be applied.',obj.id);
            end
            
            % Creating the request
            apiRequest = sprintf( 'controlunit/%i/setpoint', obj.id );
            
            % Send the request
            call_result = PreHEAT_API.ApiConfig.apiPut( apiRequest, schedule2write ); 
        end
        
        function schedule = getSchedule(obj, start_date, end_date)
                        
            % Comply with the 1 month limit
            schedule = [];
            dT_max   = days(28);
            
            for start_i = start_date:dT_max:end_date
                
                end_i = min(start_i+dT_max-seconds(1),end_date);
                
                % Creating the request
                apiRequest = sprintf( 'controlunit/%i/setpoint?start_time=%s&end_time=%s',...
                    obj.id, ...
                    PreHEAT_API.ApiConfig.convertToApiInputDate(start_i),...
                    PreHEAT_API.ApiConfig.convertToApiInputDate(end_i) ...
                    );
                
                % Send the request
                call_result = PreHEAT_API.ApiConfig.apiGet( apiRequest );
                
                % Format the result to a timetable
                if ~isempty(call_result)
                    schedule_table            = struct2table(call_result);
                    schedule_table.startTime  = PreHEAT_API.ApiConfig.convertFromApiOutputDate(schedule_table.startTime);
                    
                    schedule = [ schedule ; table2timetable(schedule_table,'RowTimes','startTime')];
                end
            end
        end
        
        function [is_active] = isActive(obj)
            is_active = obj.active;
        end
        
        % Added for backwards compatibility
        function [controller_type] = controlType(obj)
            controller_type = obj.getSubType();
        end
    end
    
    methods (Access='protected')       
        function [copy_obj] = copyElement(obj)
            copy_obj = copyElement@PreHEAT_API.BuildingUnit(obj);
        end
    end
end
