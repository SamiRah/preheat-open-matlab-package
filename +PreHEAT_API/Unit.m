
classdef (Abstract) Unit < handle & matlab.mixin.Copyable
    %unit Defines a unit in the PreHEAT sense
    
    properties (GetAccess='public', SetAccess='protected')
        id          = []        % Identifier of the unit (integer)
        name        = ''        % Name of the unit (string)
        components  = struct()  % Struct of components in the unit
        type        = ''        % Type of Unit (string)
        data        = [];       % Data for the unit
        
        locationId	= NaN;% When created from a building, this records the location id.  
    end
    
    properties (GetAccess='private', SetAccess='private')
        subType     = ''; % Details for units with different sub-types (e.g. heating secondary) - accessed via getSubType()
    end
    
    
    properties (GetAccess='protected', SetAccess='protected')
        ignoredElements  = struct();	% Struct of components in the unit
        creationStruct   = [];
    end
    
    methods (Access='public')
        function obj = Unit(struct_unit,unit_type,location_id, fields2ignore) 
            % Constructor creating a Data object out of a structure with following fields
            %
            % Inputs:
            %       struct_unit  Structure containing the relevant fields
            %                       for the desired object create among:
            %                           id                  Id of the unit (integer)
            %                           name                Name of the unit(string)
            %                           components          Array of component structures (array of struct(name,cid) )
            %
                        
            % If the structure is empty, create an empty unit
            if nargin==0 || isempty(struct_unit) 
                return
            elseif length(struct_unit)>1
                error('There can be maximum one element per Unit in the call to the constructor.');
            end
            
            if nargin>=2
                obj.type = unit_type;
            end
            if nargin>=3
                obj.locationId = location_id;
            end
            
            if nargin<4
                fields2ignore = {};
            end
            
            % For now, disregarding shared units (as they are not
            % properly handled by the code)
            if isfield(struct_unit,'shared') && struct_unit.shared
                
                unsupported_shared_unit = MException(...
                    'PreHEAT_API:Unit:sharedUnitNotSupported',...
                    'Shared units are not supported yet');
                throw(unsupported_shared_unit);
                
                % Fields that should be handled in the future:
                %   "shared": false,
                %   "buildingsCovered": 1,
                %   "sharedFrom": null,
                %   "sharedLocations": []
            end
            
            % Saving the creation struct
            obj.creationStruct = struct_unit;
            
            % Structure the fields in the appropriate components
            F = setdiff(fieldnames(struct_unit),fields2ignore);
                       
            for i=1:length(F)                    
                F_i = F{i};
                
                switch F_i
                    
                    case 'id'
                        obj.id = struct_unit.id;
                        
                    case 'name'
                        obj.name = struct_unit.name;
                        
                    case 'type'
                        obj.subType = struct_unit.type;
                                                
                    otherwise
                        try
                            % Skipping experimental unit types                            
                            if strcmp(obj.type,'ventilation') && startsWith(F_i,'setPoint')
                                % Skip the setpoints in ventilation units 
                                %   (this is to prevent usage of long term
                                %   use of unstable components)
                                obj.logIgnoredElement(F_i);
                            else                            
                                % By default, try to add a component
                                obj.addComponent(struct_unit.(F_i), F_i );
                            end
                        catch
                           fprintf(' "%s" must be handled automatically. \n',F_i); 
                           obj.logIgnoredElement(F_i);
                        end
                end
            end
            
            % If no name was given, then by default it becomes the id.
            if isempty(obj.name)
                obj.name = num2str(obj.id);
            end
            
            obj.data = PreHEAT_API.Data;
            
        end
                
        function loadData(obj,start_date, end_date, time_resolution, options2use)  
            % Method to load data for all components of the building unit over a given period.
            %       At generic Unit level, the data is loaded from the box data
            %       (i.e. not weather)
            %
            % Inputs:
            %       start_date      Start time of the data to load (Matlab datetime)
            %       end_date        End time of the data to load (Matlab datetime)
            %       time_resolution Time resolution of the data to load (string, with values allowed in PreHEAT_API.Data.resolution)
            %
            % Outputs:
            %       /               The data is loaded in the data attribute of the current object
            %
            if nargin<5
                options2use = struct();
            elseif ~isstruct(options2use)
                error('Input "options2use" must be a struct');
            end
            
            % Collects component ids and name of signals                        
            if ~isfield(options2use, 'limitComponents') 
                options2use.limitComponents = '*';
            end         
            component_details = obj.selectFocusComponents( options2use.limitComponents );
            
            
            % Loads the building data        
            obj.data.refreshComponentNames(component_details,'box_data');
            obj.data.loadData(...
                start_date,end_date,time_resolution );            
        end
     
        function importData(obj, data_table, start_date, end_date, time_resolution, options2use)
            
            if nargin<6
                options2use = struct();
            elseif ~isstruct(options2use)
                error('Input "options2use" must be a struct');
            end
            
            % Collects component ids and name of signals                        
            if ~isfield(options2use, 'limitComponents') 
                options2use.limitComponents = '*';
            end
            component_details = obj.selectFocusComponents( options2use.limitComponents );
            
            obj.data.importData(data_table, component_details, start_date, end_date, time_resolution);
        end
        
        function clearData(obj)
            % Method to load data for all components of the unit over a given period.
            %
            % Inputs:
            %       /
            %
            % Outputs:
            %       /  
            %
                      
            if ~isempty(obj.data)
                % Clears the data
                obj.data.clearData();
            end
        end
        
        function mergeData(obj, unit_with_extra_data)
            obj.data.mergeData(unit_with_extra_data.data);
        end
        
        function removeData(obj, criterion, detail)
            obj.data.removeData(criterion, detail);
        end
        
        function all_cids = getAllComponentIds(obj,add_prefix)  
            % Method to get all ids from the components of the unit.
            %
            % Inputs:
            %       add_prefix  Choose to add (1) or ignore (0) the unit name in the component name (boolean)
            %
            % Outputs:
            %       all_cids    table with all component ids of the unit (columns: id=component id and name=component name)
            %           
                                    
            if nargin<2
               add_prefix = false; 
            end
            componentDetails = obj.getAllComponentDetails(add_prefix);
            all_cids         = componentDetails(:,{'id','name','extendedName','locationId'});            
        end
        
        function componentDetails = getAllComponentDetails(obj,add_prefix)    
            % Method to get all ids from the components of the unit.
            %
            % Inputs:
            %       add_prefix  Choose to add (1) or ignore (0) the unit name in the component name (boolean)
            %
            % Outputs:
            %       all_cids    Dictionary of all component ids of the unit (containers.Map with key=component id and value=component structure)
            %
            
            if nargin<2
                add_prefix = false;
            end
            
            if islogical(add_prefix) || isnumeric(add_prefix)
                if add_prefix
                    prefix2add = '';
                else
                    prefix2add = obj.name;
                end
            elseif ischar(add_prefix)
                prefix2add = add_prefix;
            else
                error('Illegal "add_prefix" argument.');
            end
            
            % Initialise          
            all_fields          = fieldnames(obj.components);
            N_fields            = length(all_fields);
            
            id              = []; 
            cid             = [];
            name            = {}; 
            extendedName    = {};
            stdUnitDivisor  = []; 
            stdUnit         = {}; 
            unit            = {}; 
            type            = {};
            locationId      = [];
            componentDetails    = table(id,cid,name,extendedName,unit,stdUnitDivisor,stdUnit,type,locationId);
                        
            % Loop through all fields
            for i=1:N_fields                
                F_i = obj.components.(all_fields{i});
                
                % If there are several objects per field, postfix are added
                % to variable names
                if length(F_i)>1
                    add_postfix = 1;
                else
                    add_postfix = 0;                    
                end
                
                % Add all component ids to the list
                for j=1:length(F_i)
                    
                    F_ij = F_i(j);
                    
                    if ~isempty(F_ij)                        
                         
                        [id_i,cid_i] = F_ij.getId();
                        component_details_i = F_ij.getDetails();
                        
                        if iscell(component_details_i)
                            component_details_i = component_details_i{1};                                  
                        end
                        
                        % Create extended name of component
                        extendedName_i_j = sprintf('%s.%s',obj.type,all_fields{i});
                        
                        % Update name of component
                        if add_postfix
                            name_i_j = sprintf('%s_%i',all_fields{i},j);
                        else
                            name_i_j = all_fields{i};                                                        
                        end
                        
                        if add_prefix
                            name_i_j = sprintf('%s%s',prefix2add,name_i_j);
                        end
                        
                        % Checking that the name does not illegal characters
                        name_i_j         = PreHEAT_API.Component.validateName(name_i_j); 
                        extendedName_i_j = PreHEAT_API.Component.validateName(extendedName_i_j);
                        
                        componentDetails = [componentDetails;...
                            {id_i,cid_i,name_i_j,extendedName_i_j,component_details_i.unit,...
                            component_details_i.stdUnitDivisor,...
                            component_details_i.stdUnit,component_details_i.type,...
                            obj.locationId}];
                    end
                end
            end
        end
        
        function has_data_loaded = dataIsLoaded(obj)
            has_data_loaded = ~isempty(obj.data);
        end
                
        function [data] = getData(obj)
            data = copy(obj.data);
        end
        
        function addComponent(obj,component_details, component_name )
                        
            if isempty(component_details)
                obj.components.(component_name) = PreHEAT_API.Component.empty();
                
            elseif isa(component_details,'struct')
                obj.components.(component_name) = PreHEAT_API.Component( ...
                    component_details,obj.locationId, component_name);
                
            elseif isa(component_details,'PreHEAT_API.Component')
                % Create a copy of the argument and add it to the unit
                component2use = copy(component_details);                
                component2use.setType(component_name);
                obj.components.(component_name) = component2use;
                
            else
                error('Input "component_details" must be a struct or a PreHEAT_API.Component object.');
            end            
        end
        
        function [unit_contains_component] = hasA(obj,component_type)
            
            if isempty(obj)
                unit_contains_component = 0;
            else
                unit_contains_component = ismember(component_type,fieldnames(obj.components)) && ...
                    ~isempty(obj.components.(component_type));
            end
        end
        function [unit_type] = getType(obj)
            unit_type = obj.type;
        end
        
        function [state_table] = getState(obj,max_delay_allowed)
            %getState   Returns the state of a unit.
            %
            % Valid calls:
            %   [state_table] = getState(obj)
            %   [state_table] = getState(obj, max_delay_allowed)
            %
            %   state_table = table(component,value,last_update)
            %
            
            var_names_table = {'component','value','last_update'};
            state_table = table({},[],[],'VariableNames',var_names_table);
            
            component_types = obj.allComponentTypes();
            
            for i=1:length(component_types)
                component_type_i = component_types{i};
                
                if ~isempty(obj.components.(component_type_i))
                    [state_i,updated_i] = obj.components.(component_type_i).getState();
                                        
                    if isempty(state_table)
                        state_table = table({component_type_i},state_i,updated_i,'VariableNames',var_names_table);
                    else
                        state_table(end+1,:) = {component_type_i,state_i,updated_i};
                    end
                end
            end
            
            if nargin>=2 && ~isempty(state_table)
                k_out_of_date = (state_table.last_update<(datetime('now','TimeZone','UTC')-max_delay_allowed));
                state_table{k_out_of_date,'value'} = NaN;
            end
        end        
        function updateState(obj, varargin)
            %updateState Updates the state of a component
            %
            % Valid calls:
            %   updateState(obj)
            %   updateState(obj, time_resolution)
            %   updateState(obj, time_resolution, update_time)
            %   updateState(obj, time_resolution, update_time, expiry_limit)
            %   updateState(obj, 'average', averaging_period)
            %   updateState(obj, 'average', averaging_period, update_time)
            %
            % Precisions:
            % - 'average' mode works with raw data
            % - the defaut value for "update_time" is the current time
            %
                        
            T_components = obj.getAllComponentDetails();         
            if isempty(T_components)
                return;
            else
                all_cids = T_components.id;
            end
            
            if nargin>1 && strcmp(varargin{1},'average')
                import_type = 'average';
                averaging_period = varargin{2};
                if nargin<4
                    update_time = datetime('now','TimeZone','UTC');
                else
                    update_time = varargin{3};
                end
                data_table = PreHEAT_API.Data.fetchComponentData(all_cids, ...
                    update_time-averaging_period,update_time,'raw','id');
                
            else
                import_type = 'latest';
                
                if nargin==1
                    data_table  = PreHEAT_API.Data.fetchComponentData(all_cids, 'latest_id');
                elseif nargin==2
                    time_resolution = varargin{1};
                    data_table  = PreHEAT_API.Data.fetchComponentData(all_cids, 'latest_id', time_resolution);
                elseif nargin>=3
                    time_resolution = varargin{1};
                    update_time = varargin{2};
                    if nargin<4
                        expiry_limit = hours(1);
                    else
                        expiry_limit = varargin{3};
                    end
                    start_date = update_time - expiry_limit;
                    data_table = PreHEAT_API.Data.fetchComponentData(all_cids, start_date, update_time, time_resolution, 'latest_id');
                    
                else
                    error('Not supposed to be here.');
                end
            end
            
            obj.importState(data_table,import_type);            
        end
        function importState(obj, data_table, import_type)
            
            if nargin<3
                import_type = 'latest';
            end
            
            if isempty(data_table)
                return
            elseif ~istable(data_table)
                error('Input "data_table" must be a table.');
            end
            
            component_types = obj.allComponentTypes();
            
            for i=1:length(component_types)
                % Import data for all components
                component_type_i = component_types{i};
                
                if ~isempty(obj.components.(component_type_i))
                    obj.components.(component_type_i).importState(data_table,import_type);
                end
            end
        end
        
        function [api_type] = getSubType(obj)
            api_type = obj.subType;
        end
    end
    
    methods (Access='protected')
        
        function logIgnoredElement(obj, element_name)
            obj.ignoredElements.(element_name) = 1;
        end
        
        function [focus_component_details] = selectFocusComponents( obj, focus_components, add_prefix )
                            
            if nargin<3
                add_prefix = false;
            end
            
            component_details = obj.getAllComponentDetails(add_prefix);
            
            focus_component_details = PreHEAT_API.Unit.isolateFocusComponents(...
                component_details, focus_components);
        end
        
        function [copy_of_obj] = copyElement(obj)
            copy_of_obj = copyElement@matlab.mixin.Copyable(obj);
            
            % Deep copy all the components in the unit
            components_list = fieldnames(obj.components);
            for i=1:length(components_list)
                c_i = components_list{i};
                copy_of_obj.components.(c_i) = copy(obj.components.(c_i));
            end
            
            % Do not copy the data
            copy_of_obj.data = PreHEAT_API.Data;
        end
        
        function [component_types] = allComponentTypes(obj)
            component_types = fieldnames(obj.components);
            if ischar(component_types)
                component_types = { component_types };
            end
        end
    end
    
    methods (Static, Access='protected')
        
        function [options2use] = autocompleteOptionsLoading(options2use)
            
            if ~isstruct(options2use)
               error('options2use must be a struct'); 
            end
            
            % Collects component ids and name of signals                        
            if ~isfield(options2use, 'limitComponents') 
                options2use.limitComponents = '*';
            end
            if ~isfield(options2use, 'addPrefix') 
                options2use.addPrefix = 0;
            end
        end
        
        function [focus_component_details] = isolateFocusComponents(component_details, focus_components)
            
            % If not having a specific list of focus components
            if isempty(focus_components)
                focus_component_details = component_details([],:);
                return;
            elseif ischar(focus_components) && ...
                    ( strcmp(focus_components,'*') || strcmp(focus_components,'all') )
                focus_component_details = component_details;
                return;
            end
            
            % If focused on specific elements by fullname
            k_selected = find(ismember(component_details.extendedName, focus_components));
            
            % Adding components where name is specified with a wildcard
            starred_components = focus_components(endsWith(focus_components,'.*'));
            for i=1:length(starred_components)
                components_prefix_i = starred_components{i}(1:end-1);
                k_selected_i = find(startsWith(component_details.extendedName,components_prefix_i));
                if ~isempty(k_selected_i)
                    k_selected = [k_selected ; k_selected_i];
                end
            end
            
            % Isolating the components
            focus_component_details = component_details(k_selected,:);
        end
    end
end

