classdef Zone < handle
    %unit Defines a building zone in the PreHEAT sense
    
    properties (GetAccess='public', SetAccess='protected')
        id                  = NaN;  % Identifier of the zone (integer)
        name                = '';   % Name of the zone (string) 
        externalIdentifier  = '';   % Denomination of the zone in external system (string)
        area                = NaN;  % Floor area of the zone (double)
        hasExternalWall     = NaN;  % Whether the zone has an external wall (1) or not (0), NaN indicates no knowledge of this.
        subZones            = [];   % Sub-zones of the zone (PreHEAT_API.Zone array)
        adjacentZones       = [];   % Adjacent zones of the zone (PreHEAT_API.Zone array of references)
    end
   
    properties (GetAccess='protected', SetAccess='protected')
        adjacentZonesId     = [];   % Adjacent zones of the zone (array of int)
        parentBuilding      = [];   % Reference to the parent building (PreHEAT_API.Building)
        linkedUnits         = struct();
        hasVentilationSupply = NaN;
        hasVentilationExhaust = NaN;
        type = '';
    end
    
    methods (Access='public')
        function obj = Zone(zoneStruct,parent_building)
            % Constructor creating a Zone object out of a structure.
            %
            % Inputs:
            %       zoneStruct  Structure containing the relevant fields
            %                       for the desired object create among:
            %                           id                  Id of the zone (integer)
            %                           name                Name of the zone(string)
            %                           subZones            Array of structure (same format as zoneStruct)
            %                           area / zoneArea     Secondaries of the unit (array of PreHEAT_API.Unit)
            %                           hasExternalWall     Indicating whether the zone has an external wall (0/1/NaN)
            %
            %       parent_building     Handle to parent building
            %
            
            if nargin<1 || ~isstruct(zoneStruct)
                return;
            end
            if nargin<2
                parent_building = [];
            end
            
            fields2use = fieldnames(zoneStruct);
            
            for j=1:length(fields2use)
                
                field_j = fields2use{j};
                                
                switch field_j                    
                    case 'id'
                        if ~isempty(zoneStruct.id)
                            obj.id = zoneStruct.id;
                        end
                        
                    case 'name'
                        if ~isempty(zoneStruct.name)
                            obj.name = zoneStruct.name;
                        else
                            obj.name = sprintf('Zone_%i',zoneStruct.id);
                        end
                        
                    case 'area'
                        if ~isempty(zoneStruct.area)
                            obj.area = zoneStruct.area;
                        end
                        
                    case 'zoneArea'
                        if ~isempty(zoneStruct.zoneArea)
                            obj.area = zoneStruct.zoneArea;
                        end
                        
                    case 'hasExternalWall'
                        if ~isempty(zoneStruct.hasExternalWall)
                            obj.hasExternalWall = zoneStruct.hasExternalWall;
                        end
                        
                    case 'subZones'
                        subZones = PreHEAT_API.Zone.empty;
                        for i=1:length(zoneStruct.subZones)
                            subZone_i   = zoneStruct.subZones(i);
                            subZones(i) = PreHEAT_API.Zone(subZone_i,parent_building);
                        end
                        obj.subZones = subZones;
                        
                    case 'adjacentZones'
                        obj.adjacentZonesId = [];
                        for i=1:length(zoneStruct.adjacentZones)
                            obj.adjacentZonesId(end+1) = zoneStruct.adjacentZones(i).zoneId;
                        end
                        
                    case 'comfortProfileId'
                        % Do nothing (deprecated for now)
                                                
                    case 'externalIdentifier'
                        obj.externalIdentifier = zoneStruct.externalIdentifier;
                        
                    case 'ventilationSupply'
                        obj.hasVentilationSupply = zoneStruct.ventilationSupply;
                        
                    case 'ventilationExhaust'
                        obj.hasVentilationExhaust = zoneStruct.ventilationExhaust;
                        
                    case 'type'
                        if ~isempty(zoneStruct.type)
                            obj.type = zoneStruct.type;
                        end
                        
                    otherwise
                        PreHEAT_API.ApiConfig.flagUpdateRequired(...
                            sprintf('Loading of field "%s" is not yet implemented.', field_j),...
                            'Zone' );     
                end
            end
            
            obj.parentBuilding = parent_building;
                        
            obj.orderElementsById();
        end
                
        function linkAdjacentZones(obj,parent_building)
            % Creating the link to the adjacent zones out of their id in adjacentZonesId.
            %
            % Inputs:
            %
            %       parent_building     Handle to parent building
            %
            
            % Creating the references to adjacent zones
            obj.adjacentZones = PreHEAT_API.Zone.empty;
            for i=1:length(obj.adjacentZonesId)
                obj.adjacentZones(end+1) = parent_building.getZone(obj.adjacentZonesId(i));
            end
            
            % Applying the function to subZones
            for i=1:length(obj.subZones)
                obj.subZones(i).linkAdjacentZones(parent_building);
            end
        end
        
        function [details] = getZoneDetails(obj)
            % Method to load a table with all the zone details
            %
            % Inputs:
            %       /
            %
            % Outputs:
            %       details     Table containing all zone details (Matlab table(id,name,area,hasExternalWall,subZones) )
            %
            
            % Structure attributes
            details = table([obj.id],{obj.name},[obj.area],[obj.hasExternalWall],{''},...
                'VariableNames',{'id','name','area','hasExternalWall','subZones'});
            
            % Load subzones
            details_subzones = [];
            for i=1:length(obj.subZones)
                details_subzones = [ details_subzones ; obj.subZones(i).getZoneDetails() ];
            end
            
            if ~isempty(details_subzones)
                details.subZones = PreHEAT_API.ApiConfig.numericVectorToString( details_subzones.id, ',');                
            end
            
            % Concatenate result of the current zone and its subzones
            details = [ details ; details_subzones ];
        end
        
        function [zone] = getZone(obj,zone_id)
            
            zone = [];
                
            if zone_id==obj.id
                % Return the current zone, if the id fits.
                zone = obj;
                
            elseif ~isempty(obj.subZones)    
                % Else check all subzones
                for j=1:length(obj.subZones)
                    zone_j = obj.subZones(j).getZone(zone_id);
                    
                    if ~isempty(zone_j)
                        % Return the current sub-zone, if it fits.
                        zone = zone_j;
                        return;
                    end
                end
            end
        end
        
        function [building_unit] = getUnit(obj, unit_type, include_subzones)
            
            if nargin<3
                include_subzones = 1;
            end
            
            if isfield(obj.linkedUnits,unit_type)
                building_unit = obj.linkedUnits.(unit_type);
            else
                building_unit = [];
            end
            
            if include_subzones
                % Adding the units of the desired types from subzones
                for i=1:length(obj.subZones)
                    building_unit_subzones = obj.subZones(i).getUnit(unit_type, include_subzones);
                    
                    if ~isempty(building_unit_subzones)
                        building_unit = [ building_unit ; building_unit_subzones ];
                    end
                end
            end
            
            if ~iscolumn(building_unit)
                building_unit = building_unit';
            end
        end
        
        function [object_contains_component, number_of_components] = hasA(obj, unit_type, include_subzones)
            
            if nargin<3
                include_subzones = 1;
            end
            
            if ~iscell(unit_type) && ischar(unit_type)
                unit_type = {unit_type};
            end
            
            object_contains_component = zeros(length(unit_type),1);
            number_of_components      = object_contains_component;
            
            for i=1:length(unit_type)
                
                unit_type_i = unit_type{i};
                
                if isfield(obj.linkedUnits,unit_type_i)
                    object_contains_component(i) = 1;
                    number_of_components(i)      = length(obj.linkedUnits.(unit_type_i));
                end                
            end            
                            
            if include_subzones
                % Adding the units of the desired types from subzones
                for j=1:length(obj.subZones)
                    [occ_j, nc_j] = obj.subZones(j).hasA(unit_type, include_subzones);
                    object_contains_component   = object_contains_component | occ_j;
                    number_of_components        = number_of_components + nc_j;
                end
            end
        end
        
        function addLinkedUnit(obj, unit_to_link, prefix_to_add, options2use)
            
            if nargin<3 || isempty(prefix_to_add)
                unit_type = unit_to_link.type;
                prefix_to_add = '';
            else
                unit_type = strcat(prefix_to_add,'_',unit_to_link.type);
            end            
            
            if nargin<4
                % By default, the unit does not get linked to sub-zones
                options2use = struct();
            end            
            if ~isfield(options2use, 'linkToSubZones')
                options2use.linkToSubZones = 0;
            end            
            if ~isfield(options2use, 'linkSubUnits')
                options2use.linkSubUnits = 1;
            end
               
            if ~isfield(obj.linkedUnits,unit_type)
                obj.linkedUnits.(unit_type) = unit_to_link;
                
            elseif ismember( unit_to_link.id, [obj.linkedUnits.(unit_type).id] )
                % Unit is already registered, therefore nothing to do
                
            else
                obj.linkedUnits.(unit_type)(end+1) = unit_to_link;
            end
            
            if options2use.linkToSubZones
                % Then link the unit to all the sub-zones as well
                for i=1:length(obj.subZones)
                    obj.subZones(i).addLinkedUnit( unit_to_link, prefix_to_add );
                end
            end
            
            if options2use.linkSubUnits
                % Link the sub-units to the zone as well (including the prefix)
                for i=1:length(unit_to_link.subUnits)
                    obj.addLinkedUnit( unit_to_link.subUnits(i), unit_type );
                end
            end
        end
        
        function [zone_type, wet_or_dry] = getType(obj)
            %getType    Get type of zone
            %
            % Valid calls:
            %   [zone_type] = getType(obj)
            %   [zone_type, wet_or_dry] = getType(obj)
            %
            % Possible values:
            %   zone_type: APARTMENT,BATHROOM,CORRIDOR,KITCHEN,ROOM,STAIRWAY,?
            %   wet_or_dry: DRY,WET,?
            %
            
            zone_type = obj.type;
            
            if ~ischar(zone_type) || isempty(zone_type)
               zone_type = '?'; 
            end
            
            if nargout>=2
                switch zone_type
                    case {'ROOM','STAIRWAY','CORRIDOR'}
                        wet_or_dry = 'DRY';
                    case {'BATHROOM','KITCHEN'}
                        wet_or_dry = 'WET';                        
                    otherwise
                        wet_or_dry = '?';
                end
            end
        end
        
        function disp(obj)
            
            if length(obj)>1
                for i=1:length(obj)
                    obj(i).disp();
                    disp(' ');
                    disp(' ');   
                end
                return;
            end
            
            fprintf('id:                 %i \n',obj.id);
            fprintf('name:               %s \n',obj.name);
            fprintf('externalIdentifier: %s \n',obj.externalIdentifier);
            fprintf('area:               %f \n',obj.area);
            fprintf('hasExternalWall:    %i \n',obj.hasExternalWall);
            
            fprintf('adjacentZonesIds: \n');
            display_if_exist(obj.adjacentZonesId);
                        
            fprintf('adjacentZonesNames: \n');
            display_if_exist(obj.adjacentZones,'name');
            
            fprintf('subZonesIds: \n');
            display_if_exist(obj.subZones);
            
            fprintf('subZonesName: \n');
            display_if_exist(obj.subZones,'name');
            
            fprintf('linkedUnits: \n');
            display_if_exist(obj.linkedUnits);
            
            try
                fprintf('parentBuilding: %s (%i) \n',...
                    obj.parentBuilding.location.address,obj.parentBuilding.location.locationId);
            catch e
                % Do nothing
            end            
        end
    end
    
    methods (Access='protected')
        
        function orderElementsById(obj)
            
            % Sorting subUnits, zones and controlUnits
            sub_element_names = {'subZones','adjacentZones'};
            
            for i=1:length(sub_element_names)
                element_i = sub_element_names{i};
                
                % Sorting units by id
                if ~isempty(obj.(element_i))
                    elements_id = [obj.(element_i).id];
                    [~,k_sorted_elements]  = sort( elements_id );
                    obj.(element_i)  = obj.(element_i)(k_sorted_elements);
                end
            end
                     
            % Sorting the subZones by id 
            obj.adjacentZonesId  = sort( obj.adjacentZonesId );
        end
    end
end

function [] = display_if_exist(x, field2use)

if nargin<1
    field2use = '';
end

try
    if isempty(x)
        disp('[]')
    else
        if isempty(field2use)
            disp(x);
        else
            disp({x.(field2use)});
        end
    end
catch
   disp('[]') 
end

end

