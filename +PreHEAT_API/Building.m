classdef Building < handle & matlab.mixin.Copyable
    %Building Defines a building in the PreHEAT sense
    %
    % This class is a handle class and extends the PreHEAT_API.ApiConfig
    
    properties (GetAccess='public', SetAccess='protected')
        location= [];       % Structure containing the details of the location (struct)
        zones               % Array of zones within the building (PreHEAT_API.Zone array)
        units   = struct(); % Array  of units contained in the building (PreHEAT_API.Unit array)
        weather             % Weather structure for the building (PreHEAT_API.Weather)
        area                % Indoor floor area within the building, in the BBR sense (double)
        apartments          % Number of apartments within the buildings, where relevant (integer)
        type    = '';       % Type of the building (char);
    end
    
    properties (GetAccess='private', SetAccess='private')
        apiOutput = [];
    end
    
    methods (Access='public')
        
        function obj = Building(location_id)
            % Constructor creating a Building object corresponding to a
            % given location id out of an API call.
            %
            % Valid call:
            %   obj = Building(location_id)
            %
            % Inputs:
            %       location_id     Location id for the building considered (integer)
            %
            
            if nargin==0
                obj.location= [];
                obj.zones   = [];
                obj.units   = [];
                obj.weather = [];
                obj.area	= NaN;
                return;
            end
            
            if ~isnumeric(location_id)
                error('The building identifier must be a number indicating the location id.');
            end
            
            obj.loadAllDetailsFromApi(location_id);
        end
        
        function loadData(obj,start_date, end_date, time_resolution, options2use)
            %loadData   Method to load data for all components of the building over a given period.
            %
            % Valid calls:
            %   loadData(obj,start_date,end_date,time_resolution)
            %   loadData(obj,start_date,end_date,time_resolution,options2use)
            %
            % Inputs:
            %       start_date      Start time of the data to load (Matlab datetime)
            %       end_date        End time of the data to load (Matlab datetime)
            %       time_resolution Time resolution of the data to load (string, with values allowed in PreHEAT_API.Data.resolution)
            %       options2use     Structure to fine tune loading, with following fields:
            %                           loadSubcomponentsInUnit: 0 or 1 (default: 0)
            %                           limitComponents: '*' (for all) or cell of full component names (default: '*')
            %
            % Outputs:
            %       /               The data is loaded in the data attribute of all units and weatherattributes of the current object
            %                           These can be accessed with the method getData.
            
            if nargin<5
                options2use = struct();
            elseif isnumeric(options2use)
                % For backwards compatibility
                load_subcomponents_in_unit = options2use;
                options2use.loadSubcomponentsInUnit = load_subcomponents_in_unit;
            end
            
            options2use = PreHEAT_API.Building.autocompleteOptionsLoading(options2use);    
            
            
            % Loading weather data
            options2use_i = PreHEAT_API.Building.limitComponentsUnit(options2use, obj.weather);
            obj.weather.loadData(start_date, end_date, time_resolution, options2use_i);
            
            % Loading data for all of the units and their subunits
            component_details = obj.getAllComponentIds();         
            component_ids = PreHEAT_API.Building.limitComponentsBuilding( options2use, component_details );
            
            if isempty(component_ids)
                % If there is no component, do nothing
                return;
            end
            
            data_table = PreHEAT_API.Data.fetchComponentData(component_ids.id, start_date, end_date, time_resolution,'id');
            
            unit_names = fieldnames(obj.units);
            
            % Merges data from all subcomponents components
            for k=1:length(unit_names)
                
                unit_k = unit_names{k};
                
                for i = 1:length(obj.units.(unit_k))
                    obj.units.(unit_k)(i).importData(data_table, start_date, end_date, time_resolution,options2use_i);
                end
            end    
        end
        
        function importData(obj, box_data_table, weather_data_table, start_date, end_date, time_resolution, options2use)
            %importData     Method to import data for all components using data from a
            %  table with many components
            %
            % Valid calls:
            %   importData(obj, box_data_table, weather_data_table, start_date, end_date, time_resolution)
            %   importData(obj, box_data_table, weather_data_table, start_date, end_date, time_resolution, options2use)
            %
            % Inputs:
            %       start_date      Start time of the data to load (Matlab datetime)
            %       end_date        End time of the data to load (Matlab datetime)
            %       time_resolution Time resolution of the data to load (string, with values allowed in PreHEAT_API.Data.resolution)
            %
            % Outputs:
            %       /               The data is loaded in the data attribute of all units and weatherattributes of the current object
            %                           These can be accessed with the method getData.
            %
            
            if nargin<7
                options2use = struct();
            elseif isnumeric(options2use)
                % For backwards compatibility
                load_subcomponents_in_unit = options2use;
                options2use.loadSubcomponentsInUnit = load_subcomponents_in_unit;
            end
            
            options2use = PreHEAT_API.Building.autocompleteOptionsLoading(options2use);            
            
            if ~isempty(weather_data_table)
                % Loading weather data
                if ismember('loc_id',weather_data_table.Properties.VariableNames)
                    
                    if ismember('epochtime',weather_data_table.Properties.VariableNames)
                        column_time = 'epochtime';
                        
                    elseif ismember('time',weather_data_table.Properties.VariableNames)
                        column_time = 'time';
                        
                    else
                        error('No time column in data');
                    end
                    
                    selected_weather_data = weather_data_table(weather_data_table.loc_id==obj.location.locationId, ...
                        {'id',column_time,'value'});
                else
                    selected_weather_data = weather_data_table;
                end
                                
                options2use_i = PreHEAT_API.Building.limitComponentsUnit(options2use, obj.weather);
                obj.weather.importData(selected_weather_data, start_date, end_date, time_resolution, options2use_i);
            end
            
            if ~isempty(box_data_table)
                % Loading data for all of the units
                
                unit_names = fieldnames(obj.units);
                
                % Merges data from all subcomponents components
                for k=1:length(unit_names)
                    
                    unit_k = unit_names{k};
                    
                    for i = 1:length(obj.units.(unit_k))    
                        options2use_i = PreHEAT_API.Building.limitComponentsUnit(options2use, obj.units.(unit_k)(i));
                        obj.units.(unit_k)(i).importData(box_data_table, start_date, end_date, time_resolution, options2use_i);
                    end
                end
            end
        end
        
        function clearData(obj)
            %clearData Method to clear data for all components of the building over a given period.
            %
            % Valid call:
            %    clearData(obj)
            %
            
            % Loading weather data
            obj.weather.clearData();
            
            % Loading data for all of the units
            unit_names = fieldnames(obj.units);
            
            for i = 1:length(unit_names)
                
                unit_name_i = unit_names{i};
                
                for j = 1:length(obj.units.(unit_name_i))
                    obj.units.(unit_name_i)(j).clearData();
                end
            end            
        end
        
        function mergeData(obj, building_with_extra_data)
            %mergeData      Merge data of two instances of the same building
            %
            % Valid calls:
            %   importData(obj, box_data_table, weather_data_table, start_date, end_date, time_resolution)
            %   importData(obj, box_data_table, weather_data_table, start_date, end_date, time_resolution, options2use)
            %
            
            if ~isa(building_with_extra_data,'PreHEAT_API.Building')
                error('Input "building_with_extra_data" must be a PreHEAT_API.Building');
            end
            
            % Merging data on weather units
            obj.weather.mergeData(building_with_extra_data.weather);
            
            % Merging data on other building units
            units_fields = fieldnames(obj.units);
            for i=1:length(units_fields)
                
                field_i = units_fields{i};
                
                if ~isfield(building_with_extra_data.units,field_i)
                    error('The two buildings must have the same structure (field %s is missing)',field_i);
                elseif length(obj.units.(units_fields{i}))~=length(building_with_extra_data.units.(field_i))
                    error('The two buildings must have the same structure (lengths of field %s differ)',field_i);
                end
                
                for j=1:length(obj.units.(field_i))
                    obj.units.(units_fields{i})(j).mergeData(building_with_extra_data.units.(units_fields{i})(j));
                end
            end
        end
        
        function removeData(obj, criterion, detail)
            %removeData      Removes data from a building instance
            %
            % Valid call:
            %   removeData(obj, criterion, detail)
            %
            
            % Merging data on weather units
            obj.weather.removeData(criterion, detail);
            
            % Removing data on other building units
            units_fields = fieldnames(obj.units);
            for i=1:length(units_fields)                
                field_i = units_fields{i};                                
                for j=1:length(obj.units.(field_i))
                    obj.units.(units_fields{i})(j).removeData(criterion, detail);
                end
            end
        end
        
        function data = getData(obj)
            %getData Method to returning data contained in all units and weather
            % attributes
            %
            % Valid call:
            %   data = getData(obj)
            %
            % Outputs:
            %       data  Structure with fields corresponding to all the sub-data (structure with PreHEAT_API.Data fields)
            %
            % OBS: The structure of this data is currently badly usage, so
            % it will probably be updated in the future.
            
            data = struct();
            
            % Merges data from all subcomponents components
            
            % Loading weather data
            data.weather = obj.weather.getData();
            
            % Loading data for all of the units
            get_all_subunits_data = true;
            
            fields2use = fieldnames(obj.units);
            
            for i=1:length(fields2use)
                field_i = fields2use{i};
                
                for j = 1:length(obj.units.(field_i))
                    data.(field_i)(j) = obj.units.(field_i)(j).getData(get_all_subunits_data);
                end
            end
        end
        
        function all_cids = getAllComponentIds(obj)
            %getAllComponentIds     Method to returning all the ids of the components in the
            % building in containers.Map (dictionary) format.
            %
            % Valid call:
            %   all_cids = getAllComponentIds(obj)
            %
            % Outputs:
            %       all_cids  Dictionary containing ids and name of all
            %                   components in the building (Matlab containers.Map)
            %
            % OBS: external weather forecast is not considered as a component.
            
            all_cids    = [];
            add_prefix  = false; % Adding prefix is not recommended
            load_subunits = 1;   % Loading the sub-units as well
            
            unit_names = fieldnames(obj.units);
            
            % Merges data from all subcomponents components
            for k=1:length(unit_names)
                
                unit_k = unit_names{k};
                
                for i = 1:length(obj.units.(unit_k))
                    cids_i = obj.units.(unit_k)(i).getAllComponentIds(add_prefix,load_subunits);
                    if ~isempty(cids_i)
                        all_cids = [all_cids ; cids_i];
                    end
                end
            end
        end
        
        function all_weather_type_ids = getAllWeatherTypeIds(obj)
            %getAllWeatherTypeIds      Lists all the weather type ids for a
            % building
            %
            % Valid call:
            %   all_weather_type_ids = getAllWeatherTypeIds(obj)
            %
            
            all_weather_type_ids = obj.weather.getAllComponentIds();
        end
        
        function details = getZonesDetails(obj)
            %getZonesDetails    Method to list all the details of the zones in table format.
            %
            % Valid call:
            %	[details] = getZonesDetails(obj)
            %
            % Inputs:
            %       /
            % Outputs:
            %       details     Table containing zone details (Matlab table, with fields defined in PreHEAT_API.Zone.getZoneDetails())
            %
            
            details = [];
            
            for i=1:length(obj.zones)
                details = [ details ; obj.zones(i).getZoneDetails() ];
            end
            
        end
        
        function [object_is_empty] = isempty(obj)
            %isempty    Method to evaluate whether a building object is empty.
            %
            % Valid call:
            %	[object_is_empty] = isempty(obj)
            %
            % Outputs:
            %       object_is_empty     Boolean idicating whether the object is empty (1) or well defined (0)
            %
            
            object_is_empty = isempty(obj.location) || ~isfield(obj.location, 'locationId');
        end
        
        function disp(obj)
            %disp   Displays the object
            %
            % Valid calls:
            %	disp(obj)
            %
            fprintf('Location details:\n');
            disp(obj.location);
            fprintf('\nType: %s Area: %.0f m2 / Apartments: %i \n',...
                obj.type,obj.area,obj.apartments);
            fprintf('\nUnits details:\n');
            disp(obj.units);
            fprintf('\nZones details:\n');
            disp({obj.zones.name}');
        end
        
        % Functions to get meters, loops, and zones by id
        function [unit] = getBuildingUnit( obj, unit_id, unit_type )
            %getBuildingUnit    Accessor to a specific unit within the
            % building.
            %
            % Valid calls:
            %   [unit] = getBuildingUnit( obj, unit_id )
            %   [unit] = getBuildingUnit( obj, unit_id, unit_type )
            %
            
            unit_types = fieldnames(obj.units);
            
            if nargin>=3
                if ~ischar(unit_type)
                    error('The unit type must be a char input.');
                end
                
                unit_types = intersect(unit_types,unit_type);
                if isempty(unit_types)
                    error('The requested unit type (%s) does not exist for this building.',unit_type);
                end
            end
                        
            for i=1:length(unit_types)
                
                unit_type_i = unit_types{i};
                
                for j=1:length(obj.units.(unit_type_i))
                    unit = obj.units.(unit_type_i)(j).getUnitById(unit_id);
                    
                    if ~isempty(unit)
                       return; 
                    end
                end
            end
            
            % If nothing has been found until here, then it doesn't exist.
            error('The requested unit (%i) does not exist in the building.',unit_id);
        end
                
        function [zone] = getZone(obj,zone_id)
            %getZone      Accessor to a zone of a building
            %
            % Valid calls:
            %   [zone] = getZone(obj,zone_id)
            %
            
            if isempty(zone_id)
                zone = [];
                return;
            end
            
            for i=1:length(obj.zones)
                
                zone_temp = obj.zones(i).getZone(zone_id);
                
                if ~isempty(zone_temp) && zone_temp.id==zone_id
                    zone = zone_temp;
                    return;
                end
            end
            
            % If nothing has been found until here, then it doesn't exist.
            error('The requested zone (%i) does not exist in the current object.',zone_id);
        end
                
        function [object_contains_component, number_of_components] = hasA(obj,unit_component_type)
            %hasA   Method to identify whether a building contains a specific
            % component type.
            %
            % Valid calls:
            %   [object_contains_component, number_of_components] = hasA(obj,unit_component_type)
            %
            % Inputs:
            %       unit_component_type         Name of the unit/component to look for.
            %           Valid values: 
            %               cooling, coldWater, coldWaterMeterToHotWater, controller, custom,
            %               electricity, heating, heatPumps, 
            %               hotWater, hotWaterController, hotWaterTank
            %               indoorClimate, main, spaceHeatingController, ventilation
            %               
            % Outputs:
            %       object_contains_component	Boolean indicating whether
            %           the object is contains the unit/component (1) or
            %           not (0)
            %       number_of_components        Number of elements of interest.
            %
            
            number_of_components	= 0;
            
            if strcmp(unit_component_type,'heatPump')
                unit_component_type = 'heatPumps';
            end
            
            if ismember(unit_component_type,fieldnames(obj.units))
                
                number_of_components        = length(obj.units.(unit_component_type));
                
            else
                switch unit_component_type
                    
                    case 'hotWaterTank'
                        nHotWater = length(obj.units.hotWater);
                        for i=1:nHotWater
                            for j=1:length(obj.units.hotWater(i).subUnits)
                                if strcmp(obj.units.hotWater(i).subUnits(j).type,'secondaryTank')
                                    number_of_components        = number_of_components + 1;
                                end
                            end
                        end
                        
                    case 'hotWaterController'
                        nHotWater = length(obj.units.hotWater);
                        for i=1:nHotWater
                            for j=1:length(obj.units.hotWater(i).subUnits)
                                if ~isempty(obj.units.hotWater(i).subUnits(j).controlUnits)
                                    number_of_components        = number_of_components + 1;
                                end
                            end
                        end
                        
                    case 'spaceHeatingController'
                        nSpaceHeating = length(obj.units.heating);
                        for i=1:nSpaceHeating
                            for j=1:length(obj.units.heating(i).subUnits)
                                if ~isempty(obj.units.heating(i).subUnits(j).controlUnits)
                                    number_of_components        = number_of_components + 1;
                                end
                            end
                        end
                        
                    case 'controller'
                        n_controllers = 0;
                        
                        unit_types  = fieldnames(obj.units);
                        n_unit_types= length(unit_types);
                        
                        for i=1:n_unit_types
                            
                            unit_i = obj.units.(unit_types{i});
                            
                            for j_1=1:length(unit_i)
                                
                                n_j_1 = length( unit_i(j_1).controlUnits );
                                
                                % So far, only one level of subunits is supported.
                                for j_2=1:length(unit_i(j_1).subUnits)
                                    n_j_1 = n_j_1 + length( unit_i(j_1).subUnits(j_2).controlUnits );
                                end
                                
                                n_controllers = n_controllers + n_j_1;
                            end
                        end
                        
                        number_of_components        = n_controllers;
                        
                    case 'coldWaterMeterToHotWater'
                        n_cold_water_meters  = 0;
                        n_unit_DHW = length(obj.units.hotWater);
                        
                        for i=1:n_unit_DHW
                            n_subUnits_DHW_i = length(obj.units.hotWater(i).subUnits);
                            
                            for j=1:n_subUnits_DHW_i
                                if ~isempty(obj.units.hotWater(i).subUnits(j).meters) && ...
                                        isfield(obj.units.hotWater(i).subUnits(j).meters,'coldWater')
                                    
                                    n_cold_water_meters = n_cold_water_meters + ...
                                        length(obj.units.hotWater(i).subUnits(j).meters.coldWater);
                                end
                            end
                        end
                        number_of_components = n_cold_water_meters;
                        
                    otherwise
                        error('Unknown unit/component type to look for (%s)',unit_component_type);
                end
            end
            
            object_contains_component   = (number_of_components > 0);            
        end
        
        % Functions returning characteristics of the location
        function [location_id] = getLocationId(obj)
            %getLocationId      Accessor to the location Id of a building
            %
            % Valid calls:
            %   [location_id] = getLocationId(obj)
            %
            
            location_id = obj.location.locationId;
        end
                
        function [latitude,longitude] = getCoordinates(obj)
            %getCoordinates     Returns the coordinates of the building
            %
            % Valid calls:
            %   [latitude,longitude] = getCoordinates(obj)
            % 
            latitude = obj.location.latitude;
            longitude = obj.location.longitude;
        end
        
        function [address, zipcode, country] = getAddress(obj)
            %getAddress   Accessor to the address of a building
            %
            % Valid calls:
            %	[address] = getAddress(obj)
            %   [address, zipcode] = getAddress(obj)
            %   [address, zipcode, country] = getAddress(obj)
            %
            if isempty(obj)
                address = '?';
                zipcode = '?';
                country = '?';
            else
                address = obj.location.address;
                zipcode = obj.location.zipcode;
                country = obj.location.country;
            end
        end
        
        function [organisation_name,organization_id,parent_organization_name,parent_organization_id] = getOrganization(obj)
            %getOrganization    Accessor to the organization details of a building
            %
            % Valid calls:
            %   [organisation_name] = getOrganization(obj)
            %   [organisation_name,organization_id] = getOrganization(obj)
            %   [organisation_name,organization_id,parent_organization_name] = getOrganization(obj)
            %   [organisation_name,organization_id,parent_organization_name,parent_organization_id] = getOrganization(obj)
            %
            if isempty(obj)
                organisation_name = '?';
                organization_id = NaN;
                parent_organization_name = '?';
                parent_organization_id = NaN;
            else
                organisation_name = obj.location.organizationName;
                organization_id = obj.location.organizationId;  
                parent_organization_name = obj.location.parentOrganizationName;
                parent_organization_id = obj.location.parentOrganizationId;
            end
        end
        function [location_id, address, organization, organization_id] = getLocationDetails(obj)
            %getLocationDetails     Accessor to commonly used details of a building
            %
            % Valid calls:
            %   [location_id, address] = getLocationDetails(obj)
            %   [location_id, address, organization] = getLocationDetails(obj)
            %   [location_id, address, organization, organization_id] = getLocationDetails(obj)
            %
            if isempty(obj)
                location_id = NaN;
                address = '?';
                organization = '?';
                organization_id = NaN;
            else
                location_id = obj.getLocationId();
                address = obj.getAddress();
                if nargout>=3
                    [organization,organization_id] = obj.getOrganization();
                end
            end
        end
        
        %% Deprecated functions (kept for ensuring backward-compatibility)
        function [comfort_profile] = getComfortProfile(obj,comfort_profile_id)
            %getComfortProfile  This function is DEPRECATED (i.e. do not use it)
            error('This functionality is deprecated');
        end
        
        function [water_meter] = getWaterMeter(obj,unit_id)
            %getWaterMeter  This function is DEPRECATED 
            %
            % Use instead:
            %   water_meter = obj.getBuildingUnit(unit_id,'coldWater')
            %
            
            if isempty(unit_id)
                water_meter = [];
                return;
            end
            
            water_meter = obj.getBuildingUnit( unit_id, 'coldWater' );
        end
        
        function [electricity_meter] = getElectricityMeter(obj,unit_id)
            %getElectricityMeter  This function is DEPRECATED 
            %
            % Use instead:
            %   electricity_meter = obj.getBuildingUnit(unit_id,'electricity')
            %
            
            if isempty(unit_id)
                electricity_meter = [];
                return;
            end
            
            electricity_meter = obj.getBuildingUnit( unit_id, 'electricity' );
        end
        
        function [main_meter] = getMainMeter(obj,unit_id)
            %getMainMeter  This function is DEPRECATED 
            %
            % Use instead:
            %   main_meter = obj.getBuildingUnit(unit_id,'main')
            %
            
            if isempty(unit_id)
                main_meter = [];
                return;
            end
            
            main_meter = obj.getBuildingUnit( unit_id, 'main' );
        end
        
        function [space_heating_unit] = getSpaceHeatingUnit(obj,unit_id)
            %getSpaceHeatingUnit  This function is DEPRECATED 
            %
            % Use instead:
            %   space_heating_unit = obj.getBuildingUnit(unit_id,'heating')
            %
            
            if isempty(unit_id)
                space_heating_unit = [];
                return;
            end
            
            space_heating_unit = obj.getBuildingUnit( unit_id, 'heating' );
        end
        
        function [space_heating_secondary_unit] = getSpaceHeatingSecondaryUnit(obj,unit_id)
            %getSpaceHeatingSecondaryUnit  This function is DEPRECATED 
            %
            % Use instead:
            %   space_heating_secondary_unit = obj.getBuildingUnit(unit_id,'heating')
            %
            
            if isempty(unit_id)
                space_heating_secondary_unit = [];
                return;
            end
            
            space_heating_secondary_unit = obj.getBuildingUnit( unit_id, 'heating' );
            
            if ~isempty(space_heating_secondary_unit)
                if ~strcmp(space_heating_secondary_unit.type,'secondaries')
                    space_heating_secondary_unit = [];
                    error('No secondary unit was found in building for id (%i)',unit_id);
                end
            end
        end
                
        function [space_cooling_unit] = getCoolingUnit(obj,unit_id)
            %getCoolingUnit  This function is DEPRECATED 
            %
            % Use instead:
            %   space_cooling_unit = obj.getBuildingUnit(unit_id,'cooling')
            %
            
            if isempty(unit_id)
                space_cooling_unit = [];
                return;
            end
            
            space_cooling_unit = obj.getBuildingUnit( unit_id, 'cooling' );
        end
        
        function [space_cooling_secondary_unit] = getSpaceCoolingSecondaryUnit(obj,unit_id)
            %getSpaceCoolingSecondaryUnit  This function is DEPRECATED 
            %
            % Use instead:
            %   space_cooling_secondary_unit = obj.getBuildingUnit(unit_id,'cooling')
            %
            
            if isempty(unit_id)
                space_cooling_secondary_unit = [];
                return;
            end
                        
            space_cooling_secondary_unit = obj.getBuildingUnit( unit_id, 'cooling' );
            
            if ~isempty(space_cooling_secondary_unit)
                if ~strcmp(space_cooling_secondary_unit.type,'secondaries')
                    space_cooling_secondary_unit = [];
                    error('No secondary unit was found in building for id (%i)',unit_id);
                end
            end
        end
                        
        function [water_heating_unit] = getHotWaterUnit(obj,unit_id)
            %getHotWaterUnit  This function is DEPRECATED 
            %
            % Use instead:
            %   water_heating_unit = obj.getBuildingUnit(unit_id,'hotWater')
            %
                        
            if isempty(unit_id)
                water_heating_unit = [];
                return;
            end
            
            water_heating_unit = obj.getBuildingUnit( unit_id, 'hotWater' );            
        end
        
        function [heat_pump_unit] = getHeatPump(obj, unit_id)
            %getHeatPump  This function is DEPRECATED 
            %
            % Use instead:
            %   heat_pump_unit = obj.getBuildingUnit(unit_id,'heatPumps')
            %
            
            if isempty(unit_id)
                heat_pump_unit = [];
                return;
            end
            
            heat_pump_unit = obj.getBuildingUnit( unit_id, 'heatPumps' ); 
        end
        
        function [indoor_climate_unit] = getIndoorClimateUnit(obj, unit_id)
            %getIndoorClimateUnit  This function is DEPRECATED 
            %
            % Use instead:
            %   indoor_climate_unit = obj.getIndoorClimateUnit(unit_id,'indoorClimate')
            %
            
            if isempty(unit_id)
                indoor_climate_unit = [];
                return;
            end
            
            indoor_climate_unit = obj.getBuildingUnit( unit_id, 'indoorClimate' );            
        end
        
    end
    
    methods (Access='protected')
        
        function loadAllDetailsFromApi(obj,location_id)
            % Loading the building structure
            buildingStructure = PreHEAT_API.Building.apiCallBuilding(location_id);
                        
            % Create from the building structure
            obj.setLocationDetails(buildingStructure);
            obj.createFromBuildingStructure(buildingStructure);
        end
                
        function setLocationDetails(obj,buildingStructure)
            
            % Loading the parameters
            obj.location	= buildingStructure.location;
            
            if isempty(buildingStructure.buildingArea)
                obj.area	= NaN;
            else
                obj.area	= buildingStructure.buildingArea;
            end
            
            if isempty(buildingStructure.apartments)
                obj.apartments  = NaN;
            else
                obj.apartments  = buildingStructure.apartments;
            end
        end
        
        function createFromBuildingStructure(obj,buildingStructure)
                        
            % Storing the structure in the API content object
            obj.apiOutput = buildingStructure;
            
            % Loading fields in a systematic manner (and appropriate order)
            fields2use = fieldnames(buildingStructure);
            
            % Names for all units
            all_unit_names = {...
                'main','electricity','coldWater',... % Remember to load meters first (as they are then linked in loops)
                'heating','hotWater','cooling',...   % Then heating and DHW which are used by heat pumps and ventilation
                'indoorClimate',...
                'heatPumps',...  % Then heat-pumps which are used by cooling loops
                'ventilation',...
                'custom'};            
                                   
            for j=1:length(fields2use)
                
                field_j = fields2use{j};
                                                
                switch field_j
                    case {'location','buildingArea','apartments'}
                        % Handled above already
                        
                    case 'buildingType'
                        obj.type    = buildingStructure.buildingType;
                        
                    case 'zones'
                        % Zones
                        obj.zones   = PreHEAT_API.Zone.empty;
                        N_zones     = length(buildingStructure.zones);
                        for i=1:N_zones
                            obj.zones(i) = PreHEAT_API.Zone( buildingStructure.zones(i), obj );
                        end
                        
                    case 'weatherForecast'                        
                        % Weather data
                        obj.weather     = PreHEAT_API.Weather(...
                            obj.location.locationId, buildingStructure.weatherForecast);
                        
                    case 'comfortProfiles' 
                        % (deprecated - therefore skipped)
                        
                    case all_unit_names
                        % Skip at this stage
                            
                    otherwise
                        try
                            obj.units.(field_j)(end+1)   = PreHEAT_API.BuildingUnit(buildingStructure.(field_j)(i), ...
                                obj, field_j);
                        catch e
                            switch e.identifier
                                case 'PreHEAT_API:Unit:sharedUnitNotSupported'
                                    % Nothing to do (this is meant to be disregarded)
                                otherwise
                                    PreHEAT_API.ApiConfig.flagUpdateRequired(...
                                        sprintf('Loading of unit "%s" is not yet implemented.', field_j),...
                                        'Building' );
                            end
                        end
                end
            end
            
            % Loading the units
            for j=1:length(all_unit_names)
                
                field_j = all_unit_names{j};
                
                obj.units.(field_j) = PreHEAT_API.BuildingUnit.empty;
                
                if isfield(buildingStructure, field_j)
                    
                    for i = 1:length(buildingStructure.(field_j))
                        try
                            obj.units.(field_j)(end+1)   = PreHEAT_API.BuildingUnit(buildingStructure.(field_j)(i), ...
                                obj, field_j);
                        catch e
                            switch e.identifier
                                case 'PreHEAT_API:Unit:sharedUnitNotSupported'
                                    % Nothing to do (this is meant to be disregarded)
                                otherwise
                                    PreHEAT_API.ApiConfig.flagUpdateRequired(...
                                        sprintf('Loading of unit "%s" is not yet implemented.', field_j),...
                                        'Building' );
                            end
                        end
                    end                    
                end
            end
            
            % Linking the zones
            N_zones     = length(buildingStructure.zones);
            for i=1:N_zones
                obj.zones(i).linkAdjacentZones(obj);
            end
                        
            % Order elements by id to keep consistent ordering over calls
            obj.orderElementsById();            
        end
        
        function orderElementsById(obj)
            
            % Sorting zones
            [~,k_sorted_zones] = sort( [obj.zones.id] );
            obj.zones = obj.zones(k_sorted_zones);
            
            % Sorting units
            unit_names = fieldnames(obj.units);
            
            for i=1:length(unit_names)
                unit_i = unit_names{i};
                
                % Sorting units by id
                units_id = [obj.units.(unit_i).id];
                
                if ~isempty(units_id)
                    [~,k_sorted_units]  = sort( units_id );
                    obj.units.(unit_i)  = obj.units.(unit_i)(k_sorted_units);
                end
            end
        end
                
        function [building_struct] = getApiOutput(obj)
            building_struct = obj.apiOutput;
        end
        
        function [copy_of_obj] = copyElement(obj)
            copy_of_obj = copyElement@matlab.mixin.Copyable(obj);
            
            % Destroying all references
            copy_of_obj.zones   = [];
            copy_of_obj.weather = [];
            copy_of_obj.units   = struct();
            
            % Rebuild from structure received from API 
            % (WARNING: this creates the building as received from API,
            % any change since loading will be discarded)
            copy_of_obj.createFromBuildingStructure(obj.apiOutput);            
        end
    end
    
    methods (Static, Access='protected')
        
        function [options2use] = autocompleteOptionsLoading(options2use)
            
            if ~isfield(options2use, 'loadSubcomponentsInUnit')
                options2use.loadSubcomponentsInUnit = 1;
            end
            
            if ~isfield(options2use, 'limitComponents')
                options2use.limitComponents = '*';
            end
        end
        
        function [options2use_limited] = limitComponentsUnit(options2use, unit2use)
            
            options2use_limited = options2use;
            components_to_select_from = options2use.limitComponents;
            
            if ischar(components_to_select_from) && ...
                    ( strcmp(components_to_select_from,'*') || strcmp(components_to_select_from,'all') )
                
                options2use_limited.limitComponents = components_to_select_from;
                
            elseif iscell(components_to_select_from)
                type_2_select = unit2use.type;
                options2use_limited.limitComponents = ...
                    components_to_select_from(startsWith(components_to_select_from,type_2_select));
                                
            else
                error("Argument components_to_select_from must be a char ('*' or 'all') or cell.");
            end
        end
                
        function [selected_components] = limitComponentsBuilding(options2use, components_to_select_from)
            
            components_to_select = options2use.limitComponents;
            
            if isempty(components_to_select_from)               
                selected_components = [];
                
            elseif ischar(components_to_select) && ...
                    ( strcmp(components_to_select,'*') || strcmp(components_to_select,'all') )
                
                selected_components = components_to_select_from;
                
            elseif iscell(components_to_select)
                % Select components with name fully stated
                selected_components = components_to_select_from(...
                    ismember(components_to_select_from.extendedName,components_to_select),:);
                
                % Adding components where name is specified with a wildcard
                starred_components = components_to_select(endsWith(components_to_select,'.*'));
                for i=1:length(starred_components)
                    components_prefix_i = starred_components{i}(1:end-1);                    
                    components2add_i = components_to_select_from(...
                        startsWith(components_to_select_from.extendedName,components_prefix_i),:);                    
                    if ~isempty(components2add_i)
                        selected_components = [selected_components ; components2add_i];
                    end
                end
                
            else
                error("Argument components_to_select_from must be a char ('*' or 'all') or cell.");
            end
        end
        
        function [buildingStructure] = apiCallBuilding(location_id)
            API_buildings       = PreHEAT_API.ApiConfig.apiGet( sprintf('locations/%i',location_id));
            buildingStructure   = API_buildings.building;
        end
    end
end
