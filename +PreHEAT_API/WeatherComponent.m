classdef WeatherComponent < PreHEAT_API.Component
    %unit Defines a unit in the PreHEAT sense
    
    properties
        % Inherited from PreHEAT_API.Component
    end
    
    methods (Access='public')
        function obj = WeatherComponent(input_data, location_id, component_type)
            % Constructor creating a Component based upon a component id or
            % a structure.
            %
            % Inputs:
            %       input_data  Description of the component, either
            %                       component id (integer)
            %                           or
            %                       structure ( struct(cid,name) )
            %
            
            if nargin<2
                location_id = NaN;
            end
            if nargin<3
                component_type = 'value';
            end
            
            % Initialise with parent class constructor
            obj@PreHEAT_API.Component(input_data, location_id, component_type);            
        end
        
        function loadData(obj, start_date, end_date, time_resolution)
            % Method to load data for the component over a given period.
            %
            % Inputs:
            %       start_date      Start time of the data to load (Matlab datetime)
            %       end_date        End time of the data to load (Matlab datetime)
            %       time_resolution Time resolution of the data to load (string, with values allowed in PreHEAT_API.Data.resolution)
            %
            % Outputs:
            %       /               The data is loaded in the data attribute of the current object
            %
            
            component_details = obj.getDetails();
            
            % Adapting the data to match the format for a query at Unit level
            component_details.locationId= component_details.locationId;
            component_details.name     	= {component_details.name};
            component_details.stdUnit	= {component_details.stdUnit};
            component_details.unit      = {component_details.unit};
            component_details.type      = {component_details.type};
            
            % Loads the building data
            if ~isnan(obj.locationId)                
                obj.data.refreshComponentNames(component_details,'weather_data');
                obj.data.loadData( start_date,end_date,time_resolution );
            end
        end
        
        % Redefining the methods of parent class where relevant
        function updateState(obj, time_resolution, update_time, expiry_limit)
            
            if (isempty(obj.id) || isnan(obj.id)) && (isempty(obj.cid) || isnan(obj.cid))
                return;
            end
            
            if nargin<2
                time_resolution = 'hour';
            end
            if nargin<3
                update_time = datetime('now','TimeZone','Europe/Copenhagen');
            end
            if nargin<4
                expiry_limit = hours(1);
            end
            
            start_date  = update_time - expiry_limit;
            
            data_table  = PreHEAT_API.Data.fetchWeatherData(obj.locationId,obj.id, start_date, update_time, time_resolution);
            
            obj.importState(data_table);
        end
        function importState(obj, data_table,import_type)
            
            if isempty(data_table) || isempty(obj.id)
                obj.setState(NaN, NaT('TimeZone','UTC'));
            else
                focus_data  = data_table(data_table.type_id==obj.id,:);
                if isempty(focus_data)                    
                    obj.setState(NaN, NaT('TimeZone','UTC'));
                else
                    [~,k_last] = max(focus_data.epochtime);
                    value       = focus_data.normalised_value(k_last);
                    last_update = datetime( focus_data.epochtime(k_last), 'ConvertFrom', 'epochtime', 'TimeZone', 'UTC' );
                    obj.setState(value, last_update);
                end
            end
        end
    end
end

