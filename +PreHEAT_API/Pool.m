classdef Pool < handle
    %Pool Defines a Pool of buildings in the PreHEAT sense
    
    properties (GetAccess='public', SetAccess='protected')
        name = 'Generic_pool'   % Name of the pool
        numberOfBuildings       % Number of locations in the Pool (integer)
        locationIds             % Identifiers of the locations (integer[])
        addresses               % Addresses of the locations (cell array of string)
        zipcodes                % Zipcodes of the locations (cell array of string)
        cities                  % Cities of the locations (cell arrang of strings)
        countries               % Countries of the locations (cell array of strings)        
    end
        
    properties (GetAccess='private', SetAccess='private')
        organizationId          % Organisation ids
        organizationName        % Organisation names
        parentOrganizationId    % Parent organisation ids
        parentOrganizationName  % Parent organisation names
    end
    
    properties (GetAccess='protected', SetAccess='protected')
        locationsDetails    % Structure containing the details of the locations as returned by the API (struct)
                                % Obs: Experimental        
                                
        buildings % Buildings of the pool (cell array of Buildings)   
    end
    
    methods (Access='public')
        
        function obj = Pool( location_ids, load_from_API )
            % Constructor creating the Pool of buildings to which the API key has access
            %
            % Inputs:
            %       location_ids    Location ids of the locations to include in the pool 
            %                               (optional - by default gets all possible locations)    
            %
            %       skip_call       Avoid loading the data from the API (do not use / leave to 1)
            
            if nargin<1
                location_ids = 'all';
            end
            if nargin<2
                load_from_API = 1;
            end
            
            if load_from_API
                % Make an API call to get all locations within the Pool
                call_result = PreHEAT_API.ApiConfig.apiGet('locations');
            else
                call_result = struct('locations',...
                    struct(...
                        'locationId',   [],...
                        'address',      {},...
                        'zipcode',      {},...
                        'city',         {},...
                        'country',      {},...
                        'longitude',    [],...
                        'latitude',     [],...
                        'timezone',     {} ...
                        ));
            end
            
            fields_call_result = fieldnames(call_result);
            
            for i=1:length(fields_call_result)                
                field_i = fields_call_result{i};
                                
                switch field_i
                    
                    case 'locations'
                        if isnumeric(location_ids) && ~isempty(location_ids)
                            k_focus = find(ismember([call_result.locations.locationId],location_ids));
                        else
                            k_focus = [1:length(call_result.locations)]';
                        end
                        
                        % Set parameters accordingly
                        obj.numberOfBuildings   = length(k_focus);
                        obj.locationIds         = NaN(obj.numberOfBuildings,1);
                        obj.addresses           = cell(obj.numberOfBuildings,1);
                                                
                        obj.locationsDetails    = call_result.locations(k_focus);
                        
                        obj.organizationId      = NaN(obj.numberOfBuildings,1);
                        obj.organizationName	= cell(obj.numberOfBuildings,1);
                        
                        fields_locations = fieldnames(obj.locationsDetails);
                        
                        for j=1:length(fields_locations)
                            field_ij = fields_locations{j};
                            
                            switch field_ij
                                case 'locationId'
                                    obj.locationIds	= [obj.locationsDetails.locationId]';
                                    
                                case 'address'
                                    obj.addresses	= {obj.locationsDetails.address}';
                                    
                                case 'zipcode'
                                    obj.zipcodes	= {obj.locationsDetails.zipcode}';
                                    
                                case 'city'
                                    obj.cities      = {obj.locationsDetails.city}';
                                    
                                case 'country'
                                    obj.countries	= {obj.locationsDetails.country}';
                                    
                                case {'longitude','latitude','timezone'}
                                    % Do nothing about these fields
                                    
                                case 'organizationId'
                                    obj.organizationId	= {obj.locationsDetails.organizationId}';
                                    
                                case 'organizationName'
                                    obj.organizationName	= {obj.locationsDetails.organizationName}';
                                    
                                % Adding support of parent organization
                                case 'parentOrganizationId'
                                    obj.parentOrganizationId	= {obj.locationsDetails.parentOrganizationId}';
                                case 'parentOrganizationName'
                                    obj.parentOrganizationName	= {obj.locationsDetails.parentOrganizationName}';
                                    
                                otherwise
                                    PreHEAT_API.ApiConfig.flagUpdateRequired(...
                                        sprintf('Loading of field "%s" under field "location" is not yet supported.',field_ij),...
                                        'Pool' );
                            end
                        end
            
                    otherwise
                        PreHEAT_API.ApiConfig.flagUpdateRequired(...
                            sprintf('Loading of field "%s" is not yet supported.',field_i),...
                            'Pool' );
                end
            end
                        
            % Reordering elements per location id where needed
            obj.orderElementsById();
            
            obj.buildings = cell(obj.numberOfBuildings,1);
        end
        
        function Pool_details = getDetails(obj)
            % Method returning a table containing the details of the Pool object
            %
            % Inputs:
            %       /   
            % 
            % Outputs:
            %       Pool_details   Table containing the details of the
            %                           Pool (Matlab table, with columns: id, address, zipcode, city, country)
            %
            locationId  = obj.locationIds;
            address     = obj.addresses;
            zipcode     = obj.zipcodes;
            city        = obj.cities;
            country     = obj.countries;
            Pool_details = table(locationId,address,zipcode,city,country);
        end
        
        function [building] = getBuilding(obj,building_id)
            % Method returning a table containing the details of the Pool
            % object. This method implements lazy loading of buildings.
            %
            % Inputs:
            %       building_id Location id of the building to load  
            % 
            % Outputs:
            %       building    Building object with location id
            %                       corresponding to the input parameter (building_id)
            
            for i=1:(obj.numberOfBuildings)
                if building_id==(obj.locationIds(i))
                    
                    if isempty(obj.buildings{i})
                        % Loading the building if not already loaded
                        obj.buildings{i} = PreHEAT_API.Building(obj.locationIds(i));
                    end
                    
                    building = obj.buildings{i};
                    return;
                end
            end
            
            % If nothing has been found until here, then it doesn't exist.
            error('The requested building (%i) does not exist in the current object.',building_id);
        end
    end  
    
    methods (Access='protected')
        function orderElementsById(obj)
                  
            [~,k_reordering] = sort(obj.locationIds);
            
            if ~issorted(k_reordering)
                % Reordering the order of locations by increasing id                
                obj.locationIds         = obj.locationIds(k_reordering);
                obj.addresses           = obj.addresses(k_reordering);
                obj.zipcodes            = obj.zipcodes(k_reordering);
                obj.cities              = obj.cities(k_reordering);
                obj.countries           = obj.countries(k_reordering);
                obj.locationsDetails    = obj.locationsDetails(k_reordering);
            end            
        end
    end
    
    methods (Static,Access='public') 
        
        function [summary_table] = getUnitsOverview(location_ids_2_use)
            
            import PreHEAT_API.Pool
            import PreHEAT_API.Building
            
            if nargin<1 || (ischar(location_ids_2_use) && strcmp(location_ids_2_use,'all'))
                pool2use = Pool();
                location_ids_2_use = pool2use.locationIds;
            end            
            
            N_locs = length(location_ids_2_use);
            
            if iscolumn(location_ids_2_use)
                locationId  = location_ids_2_use;
            else
                locationId  = location_ids_2_use';
            end
            address      	= cell(N_locs,1);
            zipcode      	= cell(N_locs,1);
            city            = cell(N_locs,1);
            country         = cell(N_locs,1);
            main            = NaN(N_locs,1);
            heatedArea      = main;
            apartments      = main;
            heating         = main;
            cooling         = main;
            hotWater        = heating;
            indoorClimate 	= heating;
            coldWater       = heating;
            coldWaterToHotWater = heating;
            ventilation     = heating;
            electricity     = heating;
            heatPumps       = heating;
            
            controller  	= heating;            
            DHW_tank        = heating;
            
            for i = 1:length(location_ids_2_use)
                
                building_i = Building(location_ids_2_use(i));
                
                address{i}  = building_i.location.address;
                zipcode{i}  = building_i.location.zipcode;
                city{i}     = building_i.location.city;
                country{i}  = building_i.location.country;
                                
                if ~isempty(building_i.area)
                    heatedArea(i)   = building_i.area;
                end
                
                if ~isempty(building_i.apartments)
                    apartments(i)   = building_i.apartments;
                end
                
                [~,main(i)]         = building_i.hasA('main');
                [~,heating(i)]      = building_i.hasA('heating');
                [~,cooling(i)]      = building_i.hasA('cooling');
                [~,hotWater(i)]   	= building_i.hasA('hotWater');
                [~,indoorClimate(i)]= building_i.hasA('indoorClimate');
                [~,coldWater(i)]    = building_i.hasA('coldWater');
                [~,coldWaterToHotWater(i)]	= ...
                                      building_i.hasA('coldWaterMeterToHotWater');
                [~,ventilation(i)]  = building_i.hasA('ventilation');
                [~,electricity(i)]  = building_i.hasA('electricity');
                [~,heatPumps(i)]    = building_i.hasA('heatPumps');
                                
                [~,controller(i)] 	= building_i.hasA('controller');
                
                DHW_tank(i)         = building_i.hasA('hotWaterTank');
            end
            
            summary_table = table(locationId,address,zipcode,city,country,...
                heatedArea,apartments,indoorClimate,main,heating,cooling,hotWater,...
                coldWater,ventilation,electricity,heatPumps,controller,...
                DHW_tank,coldWaterToHotWater);
            
        end
    end
end

